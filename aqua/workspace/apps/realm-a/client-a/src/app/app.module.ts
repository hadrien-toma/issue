import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, Injectable, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CanLoad, Route, RouterModule, UrlSegment } from '@angular/router';
import {
	AuthModule,
	AuthWellKnownEndpoints,

	OidcConfigService,
	OidcSecurityService,
	OpenIdConfiguration
} from 'angular-auth-oidc-client';
import { combineLatest, forkJoin, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { AppComponent } from './app.component';
import { RealmAClientAForbiddenComponent } from './components/realm-a-client-a-forbidden/realm-a-client-a-forbidden.component';
import { RealmAClientANotFoundComponent } from './components/realm-a-client-a-not-found/realm-a-client-a-not-found.component';
import { RealmAClientAPostLogoutRedirectComponent } from './components/realm-a-client-a-post-logout-redirect/realm-a-client-a-post-logout-redirect.component';
import { RealmAClientARedirectComponent } from './components/realm-a-client-a-redirect/realm-a-client-a-redirect.component';
import { RealmAClientAUnauthorizedComponent } from './components/realm-a-client-a-unauthorized/realm-a-client-a-unauthorized.component';


export interface OidcConfig {
  passedConfig: OpenIdConfiguration;
  passedAuthWellKnownEndpoints?: AuthWellKnownEndpoints;
}

export function configureAuth(
  httpClient: HttpClient,
  oidcConfigService: OidcConfigService
) {
  const setupAction$ = httpClient.get(`assets/index.json`).pipe(
    switchMap((assetsConfig: any) =>
      forkJoin([
        of(assetsConfig),
        httpClient.get<OidcConfig>(assetsConfig.stsServer),
      ])
    ),
    switchMap(([assetsConfig, oidcConfig]) => {
      const config: OpenIdConfiguration = {
        // 1️⃣ The oidcConfig fetched is used as a base...
        ...oidcConfig,
        // 2️⃣ over this base, a customization is applied at build level...
        forbiddenRoute: 'forbidden',
        postLogoutRedirectUri: `${window.location.origin}/post-logout-redirect`,
        redirectUrl: `${window.location.origin}/redirect`,
        silentRenew: true,
        silentRenewUrl: `${window.location.origin}/silent-renew.html`,
        unauthorizedRoute: 'unauthorized',
        // 3️⃣ Finally, the runtime customization layer is applied so users can always customize it on their side
        ...assetsConfig,
      };
      console.log(
        'apps/realm-a/client-a/src/app/app.module.ts',
        'configureAuth',
        { config }
      );
      return oidcConfigService.withConfig(config);
    })
  );

  return () => setupAction$.toPromise();
}

@Injectable()
export class IsAuthenticatedCanLoad implements CanLoad {
  constructor(private oidcSecurityService: OidcSecurityService) {}

  canLoad(route: Route, segments: UrlSegment[]) {
    return combineLatest([
      this.oidcSecurityService.isAuthenticated$,
    ]).pipe(
      map(([isAuthenticated]) => {
        console.log({ isAuthenticated });
        if (!isAuthenticated) {
          try {
            this.oidcSecurityService.authorize();
          } catch (error) {
            console.log('apps/realm-a/client-a/src/app/app.module.ts', {
              error,
            });
            setTimeout(() => {
              this.oidcSecurityService.authorize();
            }, 1000);
          }
          return false;
        } else {
          return true;
        }
      })
    );
  }
}

@NgModule({
  declarations: [
    AppComponent,
    RealmAClientAForbiddenComponent,
    RealmAClientANotFoundComponent,
    RealmAClientAPostLogoutRedirectComponent,
    RealmAClientARedirectComponent,
    RealmAClientAUnauthorizedComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    RouterModule.forRoot(
      [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'public-routes',
        },
        {
          path: 'public-routes',
          loadChildren: () =>
            import('@workspace/apps/realm-a/client-a/public/route').then(
              (module) => module.AppsRealmAClientAPublicRouteModule
            ),
        },
        {
          path: 'private-routes',
          canLoad: [IsAuthenticatedCanLoad],
          loadChildren: () =>
            import('@workspace/apps/realm-a/client-a/private/route').then(
              (module) => module.AppsRealmAClientAPrivateRouteModule
            ),
        },
        {
          path: 'forbidden',
          pathMatch: 'full',
          component: RealmAClientAForbiddenComponent,
        },
        {
          path: 'post-logout-redirect',
          pathMatch: 'full',
          component: RealmAClientAPostLogoutRedirectComponent,
        },
        {
          path: 'redirect',
          pathMatch: 'full',
          component: RealmAClientARedirectComponent,
        },
        {
          path: 'unauthorized',
          pathMatch: 'full',
          component: RealmAClientAUnauthorizedComponent,
        },
        {
          path: '**',
          component: RealmAClientANotFoundComponent,
        }
      ],
      {
        initialNavigation: 'enabled',
        enableTracing: false,
      }
    ),
    AuthModule.forRoot(),
  ],
  providers: [
    OidcConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configureAuth,
      deps: [HttpClient, OidcConfigService],
      multi: true,
    },
    IsAuthenticatedCanLoad,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
