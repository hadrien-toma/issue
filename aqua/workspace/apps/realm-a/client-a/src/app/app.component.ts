import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';

@Component({
  selector: 'workspace-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'realm-a-client-a';
  constructor(private oidcSecurityService: OidcSecurityService){}
  ngOnInit() {
	  this.oidcSecurityService.checkAuth();
  }
}
