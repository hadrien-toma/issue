import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-realm-a-client-a-unauthorized',
  templateUrl: './realm-a-client-a-unauthorized.component.html',
  styleUrls: ['./realm-a-client-a-unauthorized.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RealmAClientAUnauthorizedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
