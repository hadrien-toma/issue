import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealmAClientARedirectComponent } from './realm-a-client-a-redirect.component';

describe('RealmAClientARedirectComponent', () => {
  let component: RealmAClientARedirectComponent;
  let fixture: ComponentFixture<RealmAClientARedirectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RealmAClientARedirectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RealmAClientARedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
