import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-realm-a-client-a-post-logout-redirect',
  templateUrl: './realm-a-client-a-post-logout-redirect.component.html',
  styleUrls: ['./realm-a-client-a-post-logout-redirect.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RealmAClientAPostLogoutRedirectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
