import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealmAClientAUnauthorizedComponent } from './realm-a-client-a-unauthorized.component';

describe('RealmAClientAUnauthorizedComponent', () => {
  let component: RealmAClientAUnauthorizedComponent;
  let fixture: ComponentFixture<RealmAClientAUnauthorizedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RealmAClientAUnauthorizedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RealmAClientAUnauthorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
