import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealmAClientAPostLogoutRedirectComponent } from './realm-a-client-a-post-logout-redirect.component';

describe('RealmAClientAPostLogoutRedirectComponent', () => {
  let component: RealmAClientAPostLogoutRedirectComponent;
  let fixture: ComponentFixture<RealmAClientAPostLogoutRedirectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RealmAClientAPostLogoutRedirectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RealmAClientAPostLogoutRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
