import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealmAClientANotFoundComponent } from './realm-a-client-a-not-found.component';

describe('RealmAClientANotFoundComponent', () => {
  let component: RealmAClientANotFoundComponent;
  let fixture: ComponentFixture<RealmAClientANotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RealmAClientANotFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RealmAClientANotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
