import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-realm-a-client-a-not-found',
  templateUrl: './realm-a-client-a-not-found.component.html',
  styleUrls: ['./realm-a-client-a-not-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RealmAClientANotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
