import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';

@Component({
  selector: 'workspace-realm-a-client-a-redirect',
  templateUrl: './realm-a-client-a-redirect.component.html',
  styleUrls: ['./realm-a-client-a-redirect.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RealmAClientARedirectComponent implements OnInit {

	constructor(public oidcSecurityService: OidcSecurityService) {}

	ngOnInit() {
	  // this.oidcSecurityService.checkAuth().subscribe((isAuthenticated) => {
	  //   console.log('app authenticated', isAuthenticated);
	  //   const at = this.oidcSecurityService.getToken();
	  //   console.log(`Current access token is '${at}'`);
	  // });
	}
}
