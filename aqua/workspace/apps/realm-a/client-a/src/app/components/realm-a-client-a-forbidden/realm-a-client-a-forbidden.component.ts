import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-realm-a-client-a-forbidden',
  templateUrl: './realm-a-client-a-forbidden.component.html',
  styleUrls: ['./realm-a-client-a-forbidden.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RealmAClientAForbiddenComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
