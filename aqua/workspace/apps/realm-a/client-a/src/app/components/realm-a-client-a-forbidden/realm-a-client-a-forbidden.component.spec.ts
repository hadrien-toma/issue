import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealmAClientAForbiddenComponent } from './realm-a-client-a-forbidden.component';

describe('RealmAClientAForbiddenComponent', () => {
  let component: RealmAClientAForbiddenComponent;
  let fixture: ComponentFixture<RealmAClientAForbiddenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RealmAClientAForbiddenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RealmAClientAForbiddenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
