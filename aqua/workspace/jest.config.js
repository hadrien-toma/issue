module.exports = {
  projects: [
    '<rootDir>/apps/realm-a/client-a',
    '<rootDir>/libs/apps/realm-a/client-a/private/route',
    '<rootDir>/libs/apps/realm-a/client-a/private/routes/route-a/route',
    '<rootDir>/libs/apps/realm-a/client-a/private/routes/route-b/route',
    '<rootDir>/libs/apps/realm-a/client-a/public/route',
    '<rootDir>/libs/apps/realm-a/client-a/public/routes/route-z/route',
    '<rootDir>/libs/apps/realm-a/client-a/public/routes/route-y/route',
  ],
};
