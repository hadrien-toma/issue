import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppsRealmAClientAPrivateRoutesRouteARouteComponent } from './components/apps-realm-a-client-a-private-routes-route-a-route/apps-realm-a-client-a-private-routes-route-a-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsRealmAClientAPrivateRoutesRouteARouteComponent,
        children: [],
      },
    ]),
  ],
  declarations: [AppsRealmAClientAPrivateRoutesRouteARouteComponent],
})
export class AppsRealmAClientAPrivateRoutesRouteARouteModule {}
