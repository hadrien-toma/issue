import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-realm-a-client-a-private-routes-route-b-route',
  templateUrl: './apps-realm-a-client-a-private-routes-route-b-route.component.html',
  styleUrls: ['./apps-realm-a-client-a-private-routes-route-b-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsRealmAClientAPrivateRoutesRouteBRouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
