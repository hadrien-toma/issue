import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppsRealmAClientAPrivateRoutesRouteBRouteComponent } from './components/apps-realm-a-client-a-private-routes-route-b-route/apps-realm-a-client-a-private-routes-route-b-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsRealmAClientAPrivateRoutesRouteBRouteComponent,
        children: [],
      },
    ]),
  ],
  declarations: [AppsRealmAClientAPrivateRoutesRouteBRouteComponent],
})
export class AppsRealmAClientAPrivateRoutesRouteBRouteModule {}
