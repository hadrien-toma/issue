import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsRealmAClientAPrivateRoutesRouteBRouteComponent } from './apps-realm-a-client-a-private-routes-route-b-route.component';

describe('AppsRealmAClientAPrivateRoutesRouteBRouteComponent', () => {
  let component: AppsRealmAClientAPrivateRoutesRouteBRouteComponent;
  let fixture: ComponentFixture<AppsRealmAClientAPrivateRoutesRouteBRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsRealmAClientAPrivateRoutesRouteBRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsRealmAClientAPrivateRoutesRouteBRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
