import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-realm-a-client-a-private-routes-route-a-route',
  templateUrl: './apps-realm-a-client-a-private-routes-route-a-route.component.html',
  styleUrls: ['./apps-realm-a-client-a-private-routes-route-a-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsRealmAClientAPrivateRoutesRouteARouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
