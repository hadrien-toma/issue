import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsRealmAClientAPrivateRoutesRouteARouteComponent } from './apps-realm-a-client-a-private-routes-route-a-route.component';

describe('AppsRealmAClientAPrivateRoutesRouteARouteComponent', () => {
  let component: AppsRealmAClientAPrivateRoutesRouteARouteComponent;
  let fixture: ComponentFixture<AppsRealmAClientAPrivateRoutesRouteARouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsRealmAClientAPrivateRoutesRouteARouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsRealmAClientAPrivateRoutesRouteARouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
