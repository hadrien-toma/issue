import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppsRealmAClientAPrivateRouteComponent } from './components/apps-realm-a-client-a-private-route/apps-realm-a-client-a-private-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsRealmAClientAPrivateRouteComponent,
        children: [
          {
            path: '',
            redirectTo: 'route-a',
          },
          {
            path: 'route-a',
            loadChildren: () =>
              import(
                '@workspace/apps/realm-a/client-a/private/routes/route-a/route'
              ).then(
                (module) => module.AppsRealmAClientAPrivateRoutesRouteARouteModule
              ),
          },
          {
            path: 'route-b',
            loadChildren: () =>
              import(
                '@workspace/apps/realm-a/client-a/private/routes/route-b/route'
              ).then(
                (module) => module.AppsRealmAClientAPrivateRoutesRouteBRouteModule
              ),
          }
        ],
      },
    ]),
  ],
  declarations: [AppsRealmAClientAPrivateRouteComponent],
})
export class AppsRealmAClientAPrivateRouteModule {}
