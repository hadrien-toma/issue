import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsRealmAClientAPrivateRouteComponent } from './apps-realm-a-client-a-private-route.component';

describe('AppsRealmAClientAPrivateRouteComponent', () => {
  let component: AppsRealmAClientAPrivateRouteComponent;
  let fixture: ComponentFixture<AppsRealmAClientAPrivateRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsRealmAClientAPrivateRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsRealmAClientAPrivateRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
