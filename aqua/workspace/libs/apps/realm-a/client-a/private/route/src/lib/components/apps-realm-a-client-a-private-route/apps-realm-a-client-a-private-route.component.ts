import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-realm-a-client-a-private-route',
  templateUrl: './apps-realm-a-client-a-private-route.component.html',
  styleUrls: ['./apps-realm-a-client-a-private-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsRealmAClientAPrivateRouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
