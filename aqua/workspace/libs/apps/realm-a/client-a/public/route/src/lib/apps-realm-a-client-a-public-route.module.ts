import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppsRealmAClientAPublicRouteComponent } from './components/apps-realm-a-client-a-public-route/apps-realm-a-client-a-public-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsRealmAClientAPublicRouteComponent,
        children: [
          {
            path: '',
            redirectTo: 'route-z',
          },
          {
            path: 'route-z',
            loadChildren: () =>
              import(
                '@workspace/apps/realm-a/client-a/public/routes/route-z/route'
              ).then(
                (module) => module.AppsRealmAClientAPublicRoutesRouteZRouteModule
              ),
          },
          {
            path: 'route-y',
            loadChildren: () =>
              import(
                '@workspace/apps/realm-a/client-a/public/routes/route-y/route'
              ).then(
                (module) => module.AppsRealmAClientAPublicRoutesRouteYRouteModule
              ),
          }
        ],
      },
    ]),
  ],
  declarations: [AppsRealmAClientAPublicRouteComponent],
})
export class AppsRealmAClientAPublicRouteModule {}
