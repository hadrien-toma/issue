import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsRealmAClientAPublicRouteComponent } from './apps-realm-a-client-a-public-route.component';

describe('AppsRealmAClientAPublicRouteComponent', () => {
  let component: AppsRealmAClientAPublicRouteComponent;
  let fixture: ComponentFixture<AppsRealmAClientAPublicRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsRealmAClientAPublicRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsRealmAClientAPublicRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
