import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-realm-a-client-a-public-route',
  templateUrl: './apps-realm-a-client-a-public-route.component.html',
  styleUrls: ['./apps-realm-a-client-a-public-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsRealmAClientAPublicRouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
