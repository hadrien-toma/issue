import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppsRealmAClientAPublicRoutesRouteZRouteComponent } from './components/apps-realm-a-client-a-public-routes-route-z-route/apps-realm-a-client-a-public-routes-route-z-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsRealmAClientAPublicRoutesRouteZRouteComponent,
        children: [],
      },
    ]),
  ],
  declarations: [AppsRealmAClientAPublicRoutesRouteZRouteComponent],
})
export class AppsRealmAClientAPublicRoutesRouteZRouteModule {}
