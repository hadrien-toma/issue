import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsRealmAClientAPublicRoutesRouteZRouteComponent } from './apps-realm-a-client-a-public-routes-route-z-route.component';

describe('AppsRealmAClientAPublicRoutesRouteZRouteComponent', () => {
  let component: AppsRealmAClientAPublicRoutesRouteZRouteComponent;
  let fixture: ComponentFixture<AppsRealmAClientAPublicRoutesRouteZRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsRealmAClientAPublicRoutesRouteZRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsRealmAClientAPublicRoutesRouteZRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
