import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-realm-a-client-a-public-routes-route-y-route',
  templateUrl: './apps-realm-a-client-a-public-routes-route-y-route.component.html',
  styleUrls: ['./apps-realm-a-client-a-public-routes-route-y-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsRealmAClientAPublicRoutesRouteYRouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
