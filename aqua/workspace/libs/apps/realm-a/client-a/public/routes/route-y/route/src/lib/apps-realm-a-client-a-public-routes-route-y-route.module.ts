import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppsRealmAClientAPublicRoutesRouteYRouteComponent } from './components/apps-realm-a-client-a-public-routes-route-y-route/apps-realm-a-client-a-public-routes-route-y-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsRealmAClientAPublicRoutesRouteYRouteComponent,
        children: [],
      },
    ]),
  ],
  declarations: [AppsRealmAClientAPublicRoutesRouteYRouteComponent],
})
export class AppsRealmAClientAPublicRoutesRouteYRouteModule {}
