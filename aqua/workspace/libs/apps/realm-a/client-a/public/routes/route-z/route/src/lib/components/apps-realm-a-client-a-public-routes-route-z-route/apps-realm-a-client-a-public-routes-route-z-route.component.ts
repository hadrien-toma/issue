import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-realm-a-client-a-public-routes-route-z-route',
  templateUrl: './apps-realm-a-client-a-public-routes-route-z-route.component.html',
  styleUrls: ['./apps-realm-a-client-a-public-routes-route-z-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsRealmAClientAPublicRoutesRouteZRouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
