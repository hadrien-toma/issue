import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsRealmAClientAPublicRoutesRouteYRouteComponent } from './apps-realm-a-client-a-public-routes-route-y-route.component';

describe('AppsRealmAClientAPublicRoutesRouteYRouteComponent', () => {
  let component: AppsRealmAClientAPublicRoutesRouteYRouteComponent;
  let fixture: ComponentFixture<AppsRealmAClientAPublicRoutesRouteYRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsRealmAClientAPublicRoutesRouteYRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsRealmAClientAPublicRoutesRouteYRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
