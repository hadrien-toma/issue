import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, Injectable, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CanLoad, Route, RouterModule, UrlSegment } from '@angular/router';
import {
	AuthModule,
	AuthWellKnownEndpoints,
	AutoLoginGuard,
	OidcConfigService,
	OidcSecurityService,
	OpenIdConfiguration
} from 'angular-auth-oidc-client';
import { combineLatest, forkJoin, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { AppComponent } from './app.component';
import { MyRealmMyClientIdForbiddenComponent } from './components/my-realm-my-client-id-forbidden/my-realm-my-client-id-forbidden.component';
import { MyRealmMyClientIdNotFoundComponent } from './components/my-realm-my-client-id-not-found/my-realm-my-client-id-not-found.component';
import { MyRealmMyClientIdPostLogoutRedirectComponent } from './components/my-realm-my-client-id-post-logout-redirect/my-realm-my-client-id-post-logout-redirect.component';
import { MyRealmMyClientIdRedirectComponent } from './components/my-realm-my-client-id-redirect/my-realm-my-client-id-redirect.component';
import { MyRealmMyClientIdUnauthorizedComponent } from './components/my-realm-my-client-id-unauthorized/my-realm-my-client-id-unauthorized.component';


export interface OidcConfig {
  passedConfig: OpenIdConfiguration;
  passedAuthWellKnownEndpoints?: AuthWellKnownEndpoints;
}

export function configureAuth(
  httpClient: HttpClient,
  oidcConfigService: OidcConfigService
) {
  const setupAction$ = httpClient.get(`assets/index.json`).pipe(
    switchMap((assetsConfig: any) =>
      forkJoin([
        of(assetsConfig),
        httpClient.get<OidcConfig>(assetsConfig.stsServer),
      ])
    ),
    switchMap(([assetsConfig, oidcConfig]) => {
      const config: OpenIdConfiguration = {
        // 1️⃣ The oidcConfig fetched is used as a base...
        ...oidcConfig,
        // 2️⃣ over this base, a customization is applied at build level...
        forbiddenRoute: 'forbidden',
        postLogoutRedirectUri: `${window.location.origin}/post-logout-redirect`,
        redirectUrl: `${window.location.origin}/redirect`,
        silentRenew: true,
        silentRenewUrl: `${window.location.origin}/silent-renew.html`,
        unauthorizedRoute: 'unauthorized',
        // 3️⃣ Finally, the runtime customization layer is applied so users can always customize it on their side
        ...assetsConfig,
      };
      console.log(
        'apps/my-realm/my-client-id/src/app/app.module.ts',
        'configureAuth',
        { config }
      );
      return oidcConfigService.withConfig(config);
    })
  );

  return () => setupAction$.toPromise();
}

@Injectable()
export class IsAuthenticatedCanLoad implements CanLoad {
  constructor(private oidcSecurityService: OidcSecurityService) {}

  canLoad(route: Route, segments: UrlSegment[]) {
    return combineLatest([
      this.oidcSecurityService.isAuthenticated$,
    ]).pipe(
      map(([isAuthenticated]) => {
        console.log({ isAuthenticated });
        if (!isAuthenticated) {
          const postLoginRoute = '/routes/route-B'; // this route value is an example, the goal is to finally take it from localStorage for instance, to retrieve the originally asked route
          try {
            this.oidcSecurityService.authorize({
                customParams: {
                  postLoginRoute,
                },
              });
          } catch (error) {
            console.log('apps/my-realm/my-client-id/src/app/app.module.ts', {
              error,
            });
            setTimeout(() => {
              this.oidcSecurityService.authorize({
                customParams: {
                  postLoginRoute,
                },
              });
            }, 1000);
          }
          return false;
        } else {
          return true;
        }
      })
    );
  }
}

@NgModule({
  declarations: [
    AppComponent,
    MyRealmMyClientIdForbiddenComponent,
    MyRealmMyClientIdNotFoundComponent,
    MyRealmMyClientIdPostLogoutRedirectComponent,
    MyRealmMyClientIdRedirectComponent,
    MyRealmMyClientIdUnauthorizedComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    RouterModule.forRoot(
      [
        {
          path: 'public-routes',
          loadChildren: () =>
            import('@workspace/apps/my-realm/my-client-id/public-route').then(
              (module) => module.AppsMyRealmMyClientIdPublicRouteModule
            ),
        },
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'routes',
        },
        {
          path: 'routes',
          canLoad: [AutoLoginGuard],
          loadChildren: () =>
            import('@workspace/apps/my-realm/my-client-id/route').then(
              (module) => module.AppsMyRealmMyClientIdRouteModule
            ),
        },
        {
          path: 'forbidden',
          pathMatch: 'full',
          component: MyRealmMyClientIdForbiddenComponent,
        },
        {
          path: 'post-logout-redirect',
          pathMatch: 'full',
          component: MyRealmMyClientIdPostLogoutRedirectComponent,
        },
        {
          path: 'redirect',
          pathMatch: 'full',
          component: MyRealmMyClientIdRedirectComponent,
        },
        {
          path: 'unauthorized',
          pathMatch: 'full',
          component: MyRealmMyClientIdUnauthorizedComponent,
        },
        {
          path: '**',
          component: MyRealmMyClientIdNotFoundComponent,
        }
      ],
      {
        initialNavigation: 'enabled',
        enableTracing: false,
      }
    ),
    AuthModule.forRoot(),
  ],
  providers: [
    OidcConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configureAuth,
      deps: [HttpClient, OidcConfigService],
      multi: true,
    },
    IsAuthenticatedCanLoad,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
