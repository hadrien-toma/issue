import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-my-realm-my-client-id-unauthorized',
  templateUrl: './my-realm-my-client-id-unauthorized.component.html',
  styleUrls: ['./my-realm-my-client-id-unauthorized.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyRealmMyClientIdUnauthorizedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
