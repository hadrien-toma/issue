import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRealmMyClientIdPostLogoutRedirectComponent } from './my-realm-my-client-id-post-logout-redirect.component';

describe('MyRealmMyClientIdPostLogoutRedirectComponent', () => {
  let component: MyRealmMyClientIdPostLogoutRedirectComponent;
  let fixture: ComponentFixture<MyRealmMyClientIdPostLogoutRedirectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyRealmMyClientIdPostLogoutRedirectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRealmMyClientIdPostLogoutRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
