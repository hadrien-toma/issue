import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-my-realm-my-client-id-forbidden',
  templateUrl: './my-realm-my-client-id-forbidden.component.html',
  styleUrls: ['./my-realm-my-client-id-forbidden.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyRealmMyClientIdForbiddenComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
