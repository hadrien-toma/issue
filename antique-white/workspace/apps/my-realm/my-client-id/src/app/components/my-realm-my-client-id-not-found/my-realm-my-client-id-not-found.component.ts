import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-my-realm-my-client-id-not-found',
  templateUrl: './my-realm-my-client-id-not-found.component.html',
  styleUrls: ['./my-realm-my-client-id-not-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyRealmMyClientIdNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
