import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRealmMyClientIdForbiddenComponent } from './my-realm-my-client-id-forbidden.component';

describe('MyRealmMyClientIdForbiddenComponent', () => {
  let component: MyRealmMyClientIdForbiddenComponent;
  let fixture: ComponentFixture<MyRealmMyClientIdForbiddenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyRealmMyClientIdForbiddenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRealmMyClientIdForbiddenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
