import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-my-realm-my-client-id-post-logout-redirect',
  templateUrl: './my-realm-my-client-id-post-logout-redirect.component.html',
  styleUrls: ['./my-realm-my-client-id-post-logout-redirect.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyRealmMyClientIdPostLogoutRedirectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
