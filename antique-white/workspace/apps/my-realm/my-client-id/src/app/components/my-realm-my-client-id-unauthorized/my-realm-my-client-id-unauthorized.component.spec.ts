import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRealmMyClientIdUnauthorizedComponent } from './my-realm-my-client-id-unauthorized.component';

describe('MyRealmMyClientIdUnauthorizedComponent', () => {
  let component: MyRealmMyClientIdUnauthorizedComponent;
  let fixture: ComponentFixture<MyRealmMyClientIdUnauthorizedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyRealmMyClientIdUnauthorizedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRealmMyClientIdUnauthorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
