import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRealmMyClientIdRedirectComponent } from './my-realm-my-client-id-redirect.component';

describe('MyRealmMyClientIdRedirectComponent', () => {
  let component: MyRealmMyClientIdRedirectComponent;
  let fixture: ComponentFixture<MyRealmMyClientIdRedirectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyRealmMyClientIdRedirectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRealmMyClientIdRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
