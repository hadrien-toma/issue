import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRealmMyClientIdNotFoundComponent } from './my-realm-my-client-id-not-found.component';

describe('MyRealmMyClientIdNotFoundComponent', () => {
  let component: MyRealmMyClientIdNotFoundComponent;
  let fixture: ComponentFixture<MyRealmMyClientIdNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyRealmMyClientIdNotFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRealmMyClientIdNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
