import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-my-realm-my-client-id-redirect',
  templateUrl: './my-realm-my-client-id-redirect.component.html',
  styleUrls: ['./my-realm-my-client-id-redirect.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyRealmMyClientIdRedirectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
