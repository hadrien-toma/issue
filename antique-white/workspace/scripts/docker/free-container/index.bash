#!/usr/bin/env bash

set -e

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(readlink --canonicalize "${hereDir}/../../..")

startDateAsS=${1:-${START_DATE_AS_S:-$(date +%s)}}
containerName=${2}

docker ps \
    --all \
    --filter "name=${containerName}" \
| \
grep \
    --quiet "${containerName}" \
&& \
docker stop \
    "${containerName}" \
|| exit 0


docker ps \
    --all \
    --filter "name=${containerName}" \
| \
grep \
    --quiet "${containerName}" \
&& \
docker rm \
    --force \
    --volumes \
    "${containerName}" \
|| exit 0
