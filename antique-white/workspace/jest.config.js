module.exports = {
  projects: [
    '<rootDir>/apps/my-realm/my-client-id',
    '<rootDir>/libs/apps/my-realm/my-client-id/route',
    '<rootDir>/libs/apps/my-realm/my-client-id/routes/route-a/route',
    '<rootDir>/libs/apps/my-realm/my-client-id/routes/route-b/route',
    '<rootDir>/libs/apps/my-realm/my-client-id/public-route',
  ],
};
