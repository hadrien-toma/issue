import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsMyRealmMyClientIdRoutesRouteBRouteComponent } from './apps-my-realm-my-client-id-routes-route-b-route.component';

describe('AppsMyRealmMyClientIdRoutesRouteBRouteComponent', () => {
  let component: AppsMyRealmMyClientIdRoutesRouteBRouteComponent;
  let fixture: ComponentFixture<AppsMyRealmMyClientIdRoutesRouteBRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsMyRealmMyClientIdRoutesRouteBRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsMyRealmMyClientIdRoutesRouteBRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
