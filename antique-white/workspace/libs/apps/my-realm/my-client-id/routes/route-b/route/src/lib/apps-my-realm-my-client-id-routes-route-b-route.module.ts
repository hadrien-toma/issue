import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppsMyRealmMyClientIdRoutesRouteBRouteComponent } from './components/apps-my-realm-my-client-id-routes-route-b-route/apps-my-realm-my-client-id-routes-route-b-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsMyRealmMyClientIdRoutesRouteBRouteComponent,
        children: [],
      },
    ]),
  ],
  declarations: [AppsMyRealmMyClientIdRoutesRouteBRouteComponent],
})
export class AppsMyRealmMyClientIdRoutesRouteBRouteModule {}
