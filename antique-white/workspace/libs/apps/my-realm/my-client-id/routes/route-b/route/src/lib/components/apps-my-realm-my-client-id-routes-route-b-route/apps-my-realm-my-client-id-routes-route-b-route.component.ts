import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-my-realm-my-client-id-routes-route-b-route',
  templateUrl: './apps-my-realm-my-client-id-routes-route-b-route.component.html',
  styleUrls: ['./apps-my-realm-my-client-id-routes-route-b-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsMyRealmMyClientIdRoutesRouteBRouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
