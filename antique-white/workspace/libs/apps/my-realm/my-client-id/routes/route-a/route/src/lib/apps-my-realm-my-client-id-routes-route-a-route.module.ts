import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppsMyRealmMyClientIdRoutesRouteARouteComponent } from './components/apps-my-realm-my-client-id-routes-route-a-route/apps-my-realm-my-client-id-routes-route-a-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsMyRealmMyClientIdRoutesRouteARouteComponent,
        children: [],
      },
    ]),
  ],
  declarations: [AppsMyRealmMyClientIdRoutesRouteARouteComponent],
})
export class AppsMyRealmMyClientIdRoutesRouteARouteModule {}
