import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-my-realm-my-client-id-routes-route-a-route',
  templateUrl: './apps-my-realm-my-client-id-routes-route-a-route.component.html',
  styleUrls: ['./apps-my-realm-my-client-id-routes-route-a-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsMyRealmMyClientIdRoutesRouteARouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
