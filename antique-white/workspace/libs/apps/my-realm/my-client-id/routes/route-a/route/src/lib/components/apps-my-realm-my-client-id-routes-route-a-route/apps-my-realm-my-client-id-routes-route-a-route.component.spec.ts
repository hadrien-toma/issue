import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsMyRealmMyClientIdRoutesRouteARouteComponent } from './apps-my-realm-my-client-id-routes-route-a-route.component';

describe('AppsMyRealmMyClientIdRoutesRouteARouteComponent', () => {
  let component: AppsMyRealmMyClientIdRoutesRouteARouteComponent;
  let fixture: ComponentFixture<AppsMyRealmMyClientIdRoutesRouteARouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsMyRealmMyClientIdRoutesRouteARouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsMyRealmMyClientIdRoutesRouteARouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
