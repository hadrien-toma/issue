import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsMyRealmMyClientIdPublicRouteComponent } from './apps-my-realm-my-client-id-public-route.component';

describe('AppsMyRealmMyClientIdPublicRouteComponent', () => {
  let component: AppsMyRealmMyClientIdPublicRouteComponent;
  let fixture: ComponentFixture<AppsMyRealmMyClientIdPublicRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsMyRealmMyClientIdPublicRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsMyRealmMyClientIdPublicRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
