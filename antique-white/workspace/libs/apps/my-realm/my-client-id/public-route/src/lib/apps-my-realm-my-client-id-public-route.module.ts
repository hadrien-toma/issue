import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppsMyRealmMyClientIdPublicRouteComponent } from './components/apps-my-realm-my-client-id-public-route/apps-my-realm-my-client-id-public-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsMyRealmMyClientIdPublicRouteComponent,
        children: [],
      },
    ])
  ],
  declarations: [AppsMyRealmMyClientIdPublicRouteComponent],
})
export class AppsMyRealmMyClientIdPublicRouteModule {}
