import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-my-realm-my-client-id-public-route',
  templateUrl: './apps-my-realm-my-client-id-public-route.component.html',
  styleUrls: ['./apps-my-realm-my-client-id-public-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsMyRealmMyClientIdPublicRouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
