import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsMyRealmMyClientIdRouteComponent } from './apps-my-realm-my-client-id-route.component';

describe('AppsMyRealmMyClientIdRouteComponent', () => {
  let component: AppsMyRealmMyClientIdRouteComponent;
  let fixture: ComponentFixture<AppsMyRealmMyClientIdRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppsMyRealmMyClientIdRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsMyRealmMyClientIdRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
