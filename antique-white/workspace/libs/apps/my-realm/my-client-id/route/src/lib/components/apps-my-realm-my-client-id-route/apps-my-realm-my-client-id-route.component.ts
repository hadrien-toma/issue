import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workspace-apps-my-realm-my-client-id-route',
  templateUrl: './apps-my-realm-my-client-id-route.component.html',
  styleUrls: ['./apps-my-realm-my-client-id-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsMyRealmMyClientIdRouteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
