import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppsMyRealmMyClientIdRouteComponent } from './components/apps-my-realm-my-client-id-route/apps-my-realm-my-client-id-route.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AppsMyRealmMyClientIdRouteComponent,
        children: [
          {
            path: '',
            redirectTo: 'route-a',
          },
          {
            path: 'route-a',
            loadChildren: () =>
              import(
                '@workspace/apps/my-realm/my-client-id/routes/route-a/route'
              ).then(
                (module) => module.AppsMyRealmMyClientIdRoutesRouteARouteModule
              ),
          },
          {
            path: 'route-b',
            loadChildren: () =>
              import(
                '@workspace/apps/my-realm/my-client-id/routes/route-b/route'
              ).then(
                (module) => module.AppsMyRealmMyClientIdRoutesRouteBRouteModule
              ),
          }
        ],
      },
    ]),
  ],
  declarations: [AppsMyRealmMyClientIdRouteComponent],
})
export class AppsMyRealmMyClientIdRouteModule {}
