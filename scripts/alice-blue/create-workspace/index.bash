#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir="${hereDir}/../../.."

workspaceWrapperDirName=$(basename $(dirname ${hereDir}))

cd ${repoDir}

mkdir ${workspaceWrapperDirName}

cd ${repoDir}/${workspaceWrapperDirName}

npx create-nx-workspace@latest workspace --preset=empty --cli=nx --interactive=false --nx-cloud=false

cd workspace

yarn add @nrwl/angular
yarn run nx generate @nrwl/angular:application --name=frontend --style=scss --routing=true

yarn run nx generate @nrwl/angular:app-shell --client-project=frontend

yarn run nx run frontend:app-shell:production
