#!/usr/bin/env bash

set -e

hereDir=`dirname $0 | while read a; do cd $a && pwd && break; done `
workspaceDir="${hereDir}/../.."

startDateAsS=${1:-${START_DATE_AS_S:-$(date +%s)}}

pids=""
rc=0

keycloakVersion="12.0.3"

trap 'sigintTrap' 2

sigintTrap() {
    echo "pids=${pids}"
    if [ "${pids}X" != "X" ]; then
        kill -9 ${pids}
    fi
    exit 2
}

databaseContainerName="keycloak-database"
databaseContainerPassword="admin"
databaseContainerPort="5432"
databaseContainerUser="admin"
databaseContainerDatabase="keycloak"

echo "" > ${hereDir}/logs/${databaseContainerName}

"${workspaceDir}/scripts/docker/free-container/index.bash" \
    "${startDateAsS}" \
    "${databaseContainerName}"

serverContainerName="keycloak-server"
serverContainerPassword="admin"
serverContainerPort="8080"
serverContainerUser="admin"
serverContainerThemePath="${hereDir}/themes/custom-theme"

echo "" > ${hereDir}/logs/${serverContainerName}

"${workspaceDir}/scripts/docker/free-container/index.bash" \
    "${startDateAsS}" \
    "${serverContainerName}"


networkName="keycloak"

"${workspaceDir}/scripts/docker/free-network/index.bash" \
    "${startDateAsS}" \
    "${networkName}"

docker network create --driver bridge "${networkName}"

npm run fkill -- --force --silent :5432
sleep 5
docker run \
    --rm \
    -p ${databaseContainerPort}:5432 \
    --name "${databaseContainerName}" \
    --network "${networkName}" \
    -e POSTGRES_DB=${databaseContainerDatabase} \
    -e POSTGRES_USER=${databaseContainerUser} \
    -e POSTGRES_PASSWORD=${databaseContainerPassword} \
    postgres:latest &
pids="${pids} $!"

npm run fkill -- --force --silent :8080
sleep 5
docker run \
    --rm \
    -p ${serverContainerPort}:8080 \
    --network "${networkName}" \
    --name "${serverContainerName}" \
    -e DB_VENDOR=POSTGRES \
    -e DB_ADDR="${databaseContainerName}" \
    -e DB_PORT=${databaseContainerPort} \
    -e DB_DATABASE=${databaseContainerDatabase} \
    -e DB_USER=${databaseContainerUser} \
    -e DB_PASSWORD=${databaseContainerPassword} \
    -e KEYCLOAK_USER=${serverContainerUser} \
    -e KEYCLOAK_PASSWORD=${serverContainerPassword} \
    --mount type=bind,source=${serverContainerThemePath},target=/opt/jboss/keycloak/themes/custom-theme \
    quay.io/keycloak/keycloak:${keycloakVersion} \
    -b 0.0.0.0 &
pids="${pids} $!"

while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' localhost:${serverContainerPort}/auth/admin)" != "302" ]] ; do echo "Waiting Keycloak interface to be ready" && sleep 5 ; done

echo "Listening on http://localhost:${serverContainerPort}/auth"

authenticationResponse=$(curl --data "username=${serverContainerUser}&password=${serverContainerPassword}&grant_type=password&client_id=admin-cli" http://localhost:${serverContainerPort}/auth/realms/master/protocol/openid-connect/token)

token=$(echo ${authenticationResponse} | sed 's/.*access_token":"//g' | sed 's/".*//g')
echo "token=${token}"

IFS_BACKUP=${IFS}
{
    IFS=,
    read
    while read -r realmRealm realmEnabled
    do
        realmPosted="{\"realm\":\"${realmRealm}\",\"enabled\":${realmEnabled}}"
        echo "realmPosted=${realmPosted}"

        realmCreationResponse=$(curl \
            -i \
            --verbose \
            --header "Content-Type: application/json" \
            --header "Authorization: bearer ${token}" \
            -X POST \
            http://localhost:${serverContainerPort}/auth/admin/realms \
            --data "${realmPosted}" \
        )

        realmId=$(echo "${realmCreationResponse}" | grep "Location: " | grep -o '[^/]*$' | sed 's/\r//')
        echo "realmId=${realmId}"

        {
            read
            while read -r realmUserUsername realmUserEnabled
            do
                realmUserPosted="{\"username\":\"${realmUserUsername}\",\"enabled\":${realmUserEnabled}}"
                echo "realmUserPosted=${realmUserPosted}"

                realmUserCreationResponse=$(curl \
                    -i \
                    --verbose \
                    --header 'Content-Type: application/json' \
                    --header "Authorization: bearer ${token}" \
                    -X POST \
                    http://localhost:${serverContainerPort}/auth/admin/realms/${realmRealm}/users \
                    --data "${realmUserPosted}"
                )

                realmUserId=$(echo "${realmUserCreationResponse}" | grep "Location: " | grep -o '[^/]*$' | sed 's/\r//')

                {
                    read
                    while read -r realmUserResetPasswordType realmUserResetPasswordTemporary realmUserResetPasswordValue
                    do
                        realmUserResetPassword="{\"type\":\"${realmUserResetPasswordType}\",\"temporary\":${realmUserResetPasswordTemporary},\"value\":\"${realmUserResetPasswordValue}\"}"

                        realmUserResetPasswordResponse=$(curl \
                            -i \
                            -vvv \
                            --header 'Content-Type: application/json' \
                            --header "Authorization: bearer ${token}" \
                            -X PUT \
                            "http://localhost:${serverContainerPort}/auth/admin/realms/${realmRealm}/users/${realmUserId}/reset-password" \
                            --data "${realmUserResetPassword}"
                        )

                         echo "realmUserResetPasswordResponse=${realmUserResetPasswordResponse}"

                    done
                } < ${hereDir}/data/realms/${realmRealm}/users/${realmUserUsername}/reset-password/index.env
            done
        } < ${hereDir}/data/realms/${realmRealm}/users/index.env

        {
            read
            while read -r realmClientClientId realmClientRootUrl realmClientRedirectUris
            do
                realmClientPosted="{\"clientId\":\"${realmClientClientId}\",\"rootUrl\":\"${realmClientRootUrl}\",\"redirectUris\":${realmClientRedirectUris}}"
                echo "realmClientPosted=${realmClientPosted}"

                realmClientCreationResponse=$(curl \
                    -i \
                    --verbose \
                    --header 'Content-Type: application/json' \
                    --header "Authorization: bearer ${token}" \
                    -X POST \
                    http://localhost:${serverContainerPort}/auth/admin/realms/${realmRealm}/clients \
                    --data "${realmClientPosted}"
                )
                realmClientId=$(echo "${realmClientCreationResponse}" | grep "Location: " | grep -o '[^/]*$' | sed 's/\r//')
                echo "realmClientId=${realmClientId}"

                realmCustomizedClientPosted="{\"publicClient\":\"true\", \"redirectUris\": [\"http://localhost:4200/redirect\", \"http://localhost:4200/post-logout-redirect\"]}"
                curl \
                    -i \
                    --verbose \
                    --header 'Content-Type: application/json' \
                    --header "Authorization: bearer ${token}" \
                    -X PUT \
                    http://localhost:${serverContainerPort}/auth/admin/realms/${realmRealm}/clients/${realmClientId} \
                    --data "${realmCustomizedClientPosted}"
            done
        } < ${hereDir}/data/realms/${realmRealm}/clients/index.env
    done
} < ${hereDir}/data/realms/index.env
IFS=${IFS_BACKUP}

for pid in ${pids}; do
    wait ${pid} || let "rc=1"
done

exit ${rc}
