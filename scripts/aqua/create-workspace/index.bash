#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir="${hereDir}/../../.."

workspaceWrapperDirName=$(basename $(dirname ${hereDir}))

cd ${repoDir}

mkdir ${workspaceWrapperDirName}

cd ${repoDir}/${workspaceWrapperDirName}

# npx --yes create-nx-workspace@latest workspace --preset=empty --cli=nx --interactive=false --nx-cloud=false
npx create-nx-workspace@latest workspace --preset=empty --cli=nx --interactive=false --nx-cloud=false --skip-git

cp --recursive ${hereDir}/includes/scripts ${repoDir}/${workspaceWrapperDirName}/workspace/scripts

cd workspace

#yarn add @ngrx/effects @ngrx/router-store @ngrx/store angular-auth-oidc-client
yarn add @ng-select/ng-option-highlight @ng-select/ng-select angular2-toaster date-fns immer libphonenumber-js mustache @ngrx/effects @ngrx/router-store @ngrx/store ajv ajv-formats

#yarn add --dev @nrwl/angular @nrwl/devkit @nrwl/nest @nrwl/node @nrwl/workspace @ngrx/store-devtools fkill-cli json
yarn add --dev @nrwl/angular @commitlint/cli @commitlint/config-conventional @compodoc/compodoc @gitbeaker/cli @ngrx/store-devtools @rx-angular/import-graph-visualizer @nrwl/node @types/mustache fkill-cli husky@4 json param-case camel-case rimraf sort-keys tree-cli webpack-bundle-analyzer

sed --in-place 's/"nx",$/"nx", "json": "json",/g' ${repoDir}/${workspaceWrapperDirName}/workspace/package.json
yarn run json --in-place -f ${repoDir}/${workspaceWrapperDirName}/workspace/package.json -e "this.scripts.fkill = 'fkill';"

yarn run nx generate @nrwl/angular:application --directory=realm-a --name=client-a --style=scss --routing=true
cp --recursive ${hereDir}/includes/assets/index.json ${repoDir}/${workspaceWrapperDirName}/workspace/apps/realm-a/client-a/src/assets/index.json

yarn run json --in-place -f ${repoDir}/${workspaceWrapperDirName}/workspace/tsconfig.base.json -e "this.angularCompilerOptions = {};"
yarn run json --in-place -f ${repoDir}/${workspaceWrapperDirName}/workspace/tsconfig.base.json -e "this.angularCompilerOptions.fullTemplateTypeCheck = true;"
yarn run json --in-place -f ${repoDir}/${workspaceWrapperDirName}/workspace/tsconfig.base.json -e "this.angularCompilerOptions.strictTemplates = true;"
yarn run json --in-place -f ${repoDir}/${workspaceWrapperDirName}/workspace/tsconfig.base.json -e "this.angularCompilerOptions.trace = true;"

#yarn run nx generate @nrwl/angular:app-shell --client-project=realm-a-client-a
yarn run nx generate @nrwl/angular:app-shell --project=realm-a-client-a

yarn run nx generate @nrwl/angular:component --project=realm-a-client-a --changeDetection=OnPush --name=components/realm-a-client-a-forbidden --style=scss --module=app.module.ts
yarn run nx generate @nrwl/angular:component --project=realm-a-client-a --changeDetection=OnPush --name=components/realm-a-client-a-not-found --style=scss --module=app.module.ts
yarn run nx generate @nrwl/angular:component --project=realm-a-client-a --changeDetection=OnPush --name=components/realm-a-client-a-post-logout-redirect --style=scss --module=app.module.ts
yarn run nx generate @nrwl/angular:component --project=realm-a-client-a --changeDetection=OnPush --name=components/realm-a-client-a-redirect --style=scss --module=app.module.ts
yarn run nx generate @nrwl/angular:component --project=realm-a-client-a --changeDetection=OnPush --name=components/realm-a-client-a-unauthorized --style=scss --module=app.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/realm-a/client-a/private --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/realm-a/client-a/private/route --buildable --parentModule=./apps/realm-a/client-a/src/app/app.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-realm-a-client-a-private-route --changeDetection=OnPush --name=components/apps-realm-a-client-a-private-route --style=scss --module=apps-realm-a-client-a-private-route.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/realm-a/client-a/private/routes/route-a --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/realm-a/client-a/private/routes/route-a/route --buildable --parentModule=./libs/apps/realm-a/client-a/private/route/src/lib/apps-realm-a-client-a-private-route.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-realm-a-client-a-private-routes-route-a-route --changeDetection=OnPush --name=components/apps-realm-a-client-a-private-routes-route-a-route --style=scss --module=apps-realm-a-client-a-private-routes-route-a-route.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/realm-a/client-a/private/routes/route-b --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/realm-a/client-a/private/routes/route-b/route --buildable --parentModule=./libs/apps/realm-a/client-a/private/route/src/lib/apps-realm-a-client-a-private-route.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-realm-a-client-a-private-routes-route-b-route --changeDetection=OnPush --name=components/apps-realm-a-client-a-private-routes-route-b-route --style=scss --module=apps-realm-a-client-a-private-routes-route-b-route.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/realm-a/client-a/public --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/realm-a/client-a/public/route --buildable --parentModule=./apps/realm-a/client-a/src/app/app.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-realm-a-client-a-public-route --changeDetection=OnPush --name=components/apps-realm-a-client-a-public-route --style=scss --module=apps-realm-a-client-a-public-route.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/realm-a/client-a/public/routes/route-z --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/realm-a/client-a/public/routes/route-z/route --buildable --parentModule=./libs/apps/realm-a/client-a/public/route/src/lib/apps-realm-a-client-a-public-route.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-realm-a-client-a-public-routes-route-z-route --changeDetection=OnPush --name=components/apps-realm-a-client-a-public-routes-route-z-route --style=scss --module=apps-realm-a-client-a-public-routes-route-z-route.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/realm-a/client-a/public/routes/route-y --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/realm-a/client-a/public/routes/route-y/route --buildable --parentModule=./libs/apps/realm-a/client-a/public/route/src/lib/apps-realm-a-client-a-public-route.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-realm-a-client-a-public-routes-route-y-route --changeDetection=OnPush --name=components/apps-realm-a-client-a-public-routes-route-y-route --style=scss --module=apps-realm-a-client-a-public-routes-route-y-route.module.ts

yarn run nx run realm-a-client-a:app-shell:production
