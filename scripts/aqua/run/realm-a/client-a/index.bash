#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir="${hereDir}/../../../../.."

workspaceWrapperDirName=$(basename $(dirname $(dirname $(dirname ${hereDir}))))

cd ${repoDir}/${workspaceWrapperDirName}/workspace

yarn run nx serve --project=realm-a-client-a
