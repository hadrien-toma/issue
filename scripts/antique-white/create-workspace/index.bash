#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir="${hereDir}/../../.."

workspaceWrapperDirName=$(basename $(dirname ${hereDir}))

cd ${repoDir}

mkdir ${workspaceWrapperDirName}

cd ${repoDir}/${workspaceWrapperDirName}

npx create-nx-workspace@latest workspace --preset=empty --cli=nx --interactive=false --nx-cloud=false

cp --recursive ${hereDir}/includes/scripts ${repoDir}/${workspaceWrapperDirName}/workspace/scripts

cd workspace

yarn add --dev fkill-cli json

sed --in-place 's/"nx",$/"nx", "json": "json",/g' ${repoDir}/${workspaceWrapperDirName}/workspace/package.json
yarn run json --in-place -f ${repoDir}/${workspaceWrapperDirName}/workspace/package.json -e "this.scripts.fkill = 'fkill';"

yarn add @nrwl/angular angular-auth-oidc-client

yarn run nx generate @nrwl/angular:application --directory=my-realm --name=my-client-id --style=scss --routing=true
cp --recursive ${hereDir}/includes/assets/index.json ${repoDir}/${workspaceWrapperDirName}/workspace/apps/my-realm/my-client-id/src/assets/index.json

yarn run nx generate @nrwl/angular:app-shell --client-project=my-realm-my-client-id

yarn run nx generate @nrwl/angular:component --project=my-realm-my-client-id --changeDetection=OnPush --name=components/my-realm-my-client-id-forbidden --style=scss --module=app.module.ts
yarn run nx generate @nrwl/angular:component --project=my-realm-my-client-id --changeDetection=OnPush --name=components/my-realm-my-client-id-not-found --style=scss --module=app.module.ts
yarn run nx generate @nrwl/angular:component --project=my-realm-my-client-id --changeDetection=OnPush --name=components/my-realm-my-client-id-post-logout-redirect --style=scss --module=app.module.ts
yarn run nx generate @nrwl/angular:component --project=my-realm-my-client-id --changeDetection=OnPush --name=components/my-realm-my-client-id-redirect --style=scss --module=app.module.ts
yarn run nx generate @nrwl/angular:component --project=my-realm-my-client-id --changeDetection=OnPush --name=components/my-realm-my-client-id-unauthorized --style=scss --module=app.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/my-realm/my-client-id --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/my-realm/my-client-id/route --buildable --parentModule=./apps/my-realm/my-client-id/src/app/app.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-my-realm-my-client-id-route --changeDetection=OnPush --name=components/apps-my-realm-my-client-id-route --style=scss --module=apps-my-realm-my-client-id-route.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/my-realm/my-client-id/routes/route-a --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/my-realm/my-client-id/routes/route-a/route --buildable --parentModule=./libs/apps/my-realm/my-client-id/route/src/lib/apps-my-realm-my-client-id-route.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-my-realm-my-client-id-routes-route-a-route --changeDetection=OnPush --name=components/apps-my-realm-my-client-id-routes-route-a-route --style=scss --module=apps-my-realm-my-client-id-routes-route-a-route.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/my-realm/my-client-id/routes/route-b --name=route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/my-realm/my-client-id/routes/route-b/route --buildable --parentModule=./libs/apps/my-realm/my-client-id/route/src/lib/apps-my-realm-my-client-id-route.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-my-realm-my-client-id-routes-route-b-route --changeDetection=OnPush --name=components/apps-my-realm-my-client-id-routes-route-b-route --style=scss --module=apps-my-realm-my-client-id-routes-route-b-route.module.ts

yarn run nx generate @nrwl/angular:library --directory=apps/my-realm/my-client-id --name=public-route --style=scss --routing=true --lazy=true --publishable --strict --importPath=@workspace/apps/my-realm/my-client-id/public-route --buildable --parentModule=./apps/my-realm/my-client-id/src/app/app.module.ts
yarn run nx generate @nrwl/angular:component --project=apps-my-realm-my-client-id-public-route --changeDetection=OnPush --name=components/apps-my-realm-my-client-id-public-route --style=scss --module=apps-my-realm-my-client-id-public-route.module.ts

yarn run nx run my-realm-my-client-id:app-shell:production

#
# yarn run nx generate @nrwl/angular:library --directory=my-realm --name=my-client-id --style=scss --routing=true --lazy=true
