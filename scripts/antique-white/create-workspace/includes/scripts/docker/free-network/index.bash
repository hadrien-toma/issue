#!/usr/bin/env bash

set -e

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(readlink --canonicalize "${hereDir}/../../..")

startDateAsS=${1:-${START_DATE_AS_S:-$(date +%s)}}
networkName=${2}

docker network ls \
    --filter "name=${networkName}" \
| \
grep \
    --quiet "${networkName}" \
&& \
docker network remove \
    "${networkName}" \
|| exit 0