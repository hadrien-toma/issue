import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Looper } from '../reducers/index.reducer';

export const $looper = createFeatureSelector<Looper>('looper');
export const $looperDueTime = createSelector($looper, looper => looper?.dueTime);
export const $looperIsLooping = createSelector($looper, looper => looper?.isLooping);
export const $looperPeriod = createSelector($looper, looper => looper?.period);
