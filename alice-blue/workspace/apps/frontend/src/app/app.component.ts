import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { $looperDueTime, $looperIsLooping, $looperPeriod } from './selectors';

@Component({
  selector: 'workspace-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  looperDueTime$ = this.store.pipe(select($looperDueTime));
  looperIsLooping$ = this.store.pipe(select($looperIsLooping));
  looperPeriod$ = this.store.pipe(select($looperPeriod));

  constructor(private store: Store<any>){}
}
