import { createAction } from '@ngrx/store';

export const µEffectsNgrxOnInitEffectsEntered = createAction(`µEffectsNgrxOnInitEffectsEntered`);
export const µStartLooping = createAction(`µStartLooping`);
export const µLoop = createAction(`µLoop`);
