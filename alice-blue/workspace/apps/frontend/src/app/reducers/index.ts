import { InjectionToken } from '@angular/core';
import { Action, ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { Looper, looper } from './index.reducer'

export interface FeatureState {
	looper: Looper;
}

export interface State {
	app: FeatureState;
}

export const reducers = new InjectionToken<ActionReducerMap<FeatureState, Action>>('app', {
	factory: () => ({ looper })
});

export function failureLogger(reducer: ActionReducer<any>): ActionReducer<any> {
	return function <T>(state: State, action: { type: string; [key: string]: any }) {
		const isFailureAction = action.type.endsWith('failure') || action.type.endsWith('Failure');
		if (isFailureAction) {
			console.error({ ...action });
		}
		return reducer(state, action);
	};
}

export const metaReducers: MetaReducer<{}>[] = [failureLogger];
