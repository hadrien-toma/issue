import { createReducer, on } from '@ngrx/store';
import produce from 'immer';
import { µStartLooping } from '../actions';

export interface Looper {
	dueTime: number;
	isLooping: boolean;
	period: number;
};

export const looper = createReducer(
	{
		dueTime: 500,
		isLooping: false,
		period: 1000
	},
	on(
		µStartLooping,
		(state): Looper =>
			produce(state, (draft) => {
				draft.isLooping = true;
			})
	)
);
