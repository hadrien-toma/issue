import { isPlatformServer } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { combineLatest, of, timer } from 'rxjs';
import { concatMap, exhaustMap, filter, mapTo, withLatestFrom } from 'rxjs/operators';

import { µEffectsNgrxOnInitEffectsEntered, µLoop, µStartLooping } from '../actions';
import { $looperDueTime, $looperPeriod } from '../selectors';

@Injectable()
export class AppEffects implements OnInitEffects {
	µEffectsNgrxOnInitEffectsEntered__µStartLooping$ = createEffect(
		() =>
			combineLatest([this.actions$.pipe(ofType(µEffectsNgrxOnInitEffectsEntered))]).pipe(
				mapTo(µStartLooping())
			),
		{ dispatch: true }
	);

	µStartLooping__µLoop$ = createEffect(
		() =>
			combineLatest([this.actions$.pipe(ofType(µStartLooping))]).pipe(
				exhaustMap((actionArray) => of(actionArray).pipe(
				  withLatestFrom(this.store.pipe(select($looperDueTime)), this.store.pipe(select($looperPeriod)))
				)),
				filter(() => !isPlatformServer(this.platform)),
				exhaustMap(([_, looperDueTime, looperPeriod]) => timer(looperDueTime, looperPeriod)),
				mapTo(µLoop())
			),
		{ dispatch: true }
	);

	constructor(private actions$: Actions, private store: Store<any>, @Inject(PLATFORM_ID) private platform: Object) { }

	ngrxOnInitEffects() {
		return µEffectsNgrxOnInitEffectsEntered();
	}
}
