# issue

npx create-nx-workspace@latest workspace --preset=empty --cli=nx --interactive=false --nx-cloud=false

cd workspace

yarn add @nrwl/angular
yarn run nx generate @nrwl/angular:application --name=frontend --style=scss --routing=false

yarn add @nrwl/nest
yarn run nx generate @nrwl/nest:application --name=backend
yarn run nx generate @nrwl/nest:library --name=micro-service --buildable
